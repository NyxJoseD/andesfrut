<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$title = 'AndesFrut';

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Kickoff">
    <meta name="publisher" content="Kickoff">
    <meta name="description" content="AndesFrut es una empresa de comercialización, logística y asesoramiento, en la compra y distribución de productos agrícolas, para la industria y/o empresas de servicios alimentarios.">
    <meta name="keywords" content="comercialización de frutas, comercialización de hortalizas, comercialización de verduras, comercialización de cereales, logística, logística de frutas, logística de hortalizas, logística de cereales, exportación agrícola, exportación de frutas, exportación de cereales, exportación de frutas, exportación de hortalizas, asesoramiento, productos agrícolas, servicios alimentarios">
    <title>
        <?= $title ?> 
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap') ?>
    <?= $this->Html->css('style') ?>
    <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>
<body>
    <div class="container-fluid header">
        <div class="container">
            <div class="row">
                <?= $this->Html->link($this->Html->image('logo.png', ['class' => 'img-responsive logo']), '/', ['escape' => false]) ?>
                <div class="header-links">
                    <div class="langs">
                       <?= $this->Html->link($this->Html->image('es.jpg'), '/es/', ['escape' => false]) ?>
                       <?= $this->Html->link($this->Html->image('en.jpg'), '/en/', ['escape' => false]) ?>
                       <div class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $this->Html->image('cn.jpg') ?>
                        </a>
                          <ul class="dropdown-menu">  
                            <li><?= $this->Html->link('简体中文', '/zh-cn/', ['escape' => false]) ?></li>
                            <li><?= $this->Html->link('繁體中文', '/zh-tw/', ['escape' => false]) ?></li>
                          </ul> 
                       </div>
                    </div>
                    <div class="menu">
                        <a href="#" class="menu-toggle dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></a>
                        <ul class="dropdown-menu">
                          <li><?= $this->Html->link(__('Frutos de Chile'), '/fruits-of-chile/') ?></li>
                          <li><?= $this->Html->link(__('Nosotros'), '/about-us/') ?></li>
                          <li><?= $this->Html->link(__('Nuestros Principios'), '/our-principles/') ?></li>
                          <li><?= $this->Html->link(__('Servicios'), '/services/') ?></li>
                          <li><?= $this->Html->link(__('Contacto'), '/contact/') ?></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
        <?= $this->fetch('content') ?>

<div class="container-fluid footer">
    <div class="container">
        <div class="col-sm-3">
            <?= $this->Html->image('logo-alt.png', ['class' => 'img-responsive img-wide']) ?>
        </div>
        <div class="col-sm-5 col-sm-offset-4">
            <i class="fa fa-map-marker fa-fw"></i> Dr Manuel Barros Borgoño 71. Of 504, Santiago, Chile<br>
            <i class="fa fa-phone fa-fw"></i> +56 2 2929 4882<br>
            <i class="fa fa-envelope fa-fw"></i> contacto@andesfrut.com

        </div>
    </div>
</div>
</body>
</html>
    <?= $this->Html->script('jquery') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->script('bootstrap.min') ?>
    <?= $this->Html->script('js') ?>
