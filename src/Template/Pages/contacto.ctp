<?= $this->assign('title', '- ' .  __('Contacto')) ?>

<div class="container-fluid title title-frutos-de-chile">
    <div class="container">
        <h2><?= __('Contacto') ?></h2>
    </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-4">
			<p class="font-light-green"><?= __('getincontact') ?></p>
            <h4>
            	<i class="fa fa-envelope fa-fw"></i> contacto@andesfrut.com
            </h4>
		</div>
		<div class="col-sm-4">
			<p>
				<b>Santiago, Chile</b><br>
				<i class="fa fa-map-marker fa-fw"></i> Dr Manuel Barros Borgoño 71 Of.505 <br>
	            <i class="fa fa-phone fa-fw"></i> +56 2 2929 4882 <br>				
			</p>
			<p>
				<b>Curic贸, Chile</b><br>
				<i class="fa fa-map-marker fa-fw"></i> Rosario 1293 <br>
	            <i class="fa fa-phone fa-fw"></i> +56 75 2591 714 <br>				
			</p>
			<p>
				<b>Shangai, China</b><br>
				<i class="fa fa-map-marker fa-fw"></i> Xiang Yang Nan Rd 218, Room 1301, Xiang Dai Building. Shangai 200031 P.R. China <br>
	            <i class="fa fa-phone fa-fw"></i> +86-21-54 651 446 <br>				
			</p>

		</div>
		<div class="col-sm-3 col-sm-offset-1">
			<?= $this->Html->image('contacto.jpg', ['class' => 'img-responsive img-wide']) ?>
		</div>
	</div>
</div>