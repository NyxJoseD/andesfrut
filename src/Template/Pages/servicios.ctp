<?= $this->assign('title', '- ' .  __('Services')) ?>

<div class="container-fluid title title-frutos-de-chile">
    <div class="container">
        <h2><?= __('Servicios') ?></h2>
    </div>
</div>
<div class="container">
	<div class="row">
		<div class="servicio clearfix">
			<div class="col-sm-2">
				<?= $this->Html->image('servicios/regular-mode.png', ['class' => 'img-responsive img-wide']) ?>
			</div>
			<div class="col-sm-10">
        		<?php if ($locale=='es_CL') {?>
					<h4>Distribución Regular Mode</h4>
					<p>
						Comercialización y entrega de productos agropecuarios en cantidades establecidas con periodicidad constante durante períodos de tiempo prolongados (mensual, trimestral, anual, etc.) <br>
						Considera Schedule fijo de destinos y una relación permanente con la contraparte receptora del delivery.
					</p>
				<?php } elseif ($locale=='en_US') { ?>
					<h4>Regular Mode Distribution</h4>
					<p>
						Commercialization and delivery of agricultural products in established quentities for long term periods (monthly, quarterly, annually, etc).  <br>
						It considers a fixed destinations schedule and constant relations with the receiving counterparty. 
					</p>				
				<?php } elseif ($locale=='zh_CN') { ?>
					<h4>普通經銷模式</h4>
					<p>
						農産品長期（每月、每季度、每年等）定額運輸與經銷。 <br>
						考慮固定目的地，並與合約方保持穩定的合作關系。
					</p>				
				<?php } elseif ($locale=='zh_TW') { ?>
					<h4>普通经销模式</h4>
					<p>
						农产品长期（每月、每季度、每年等）定额运输与经销。 <br>
						考虑固定目的地，并与合约方保持稳定的合作关系。
					</p>				
        		<?php } ?>
			</div>
		</div>

		<div class="servicio clearfix">
			<div class="col-sm-2">
				<?= $this->Html->image('servicios/on-demand-mode.png', ['class' => 'img-responsive img-wide']) ?>
			</div>
			<div class="col-sm-10">
        		<?php if ($locale=='es_CL') {?>
					<h4>Distribución On-Demand Mode</h4>
					<p>
						Comercialización y entrega de productos agropecuarios en cantidades variables con periodicidad variable. <br>
						Este servicio requiere un proceso de cotización, proforma y contrato de envío-recepción con la contraparte técnica.
					</p>
				<?php } elseif ($locale=='en_US') { ?>
					<h4>On-Demand Mode Distribution</h4>
					<p>
						Commercialization and delivery of agricultural products in variable quantities and variable frequency. <br>
						This service requires a quotation process and sender-receiver contract with the counterparty.
					</p>
				<?php } elseif ($locale=='zh_CN') { ?>
					<h4>按需經銷模式</h4>
					<p>
						按需求量及不同的時期進行農産品的銷售和交付。 <br>
						項服務需要進行報價，並且收發工作應與合約方保持聯系。
					</p>
				<?php } elseif ($locale=='zh_TW') { ?>
					<h4>按需经销模式</h4>
					<p>
						按需求量及不同的时期进行农产品的销售和交付。<br>
						项服务需要进行报价，并且收发工作应与合约方保持联系。
					</p>
        		<?php } ?>
			</div>
		</div>

		<div class="servicio clearfix">
			<div class="col-sm-2">
				<?= $this->Html->image('servicios/spot-mode.png', ['class' => 'img-responsive img-wide']) ?>
			</div>
			<div class="col-sm-10">
        		<?php if ($locale=='es_CL') {?>
					<h4>Distribución Spot Mode</h4>
					<p>
						Comercialización y entrega de productos agropecuarios en cantidades y frecuencia estacional. <br>
						Este servicio permite a nuestros clientes acceder a volúmenes de productos agrícolas que forman parte de remanentes de stock. <br>
						Este servicio requiere de un convenio previo de compraventa, que establece bandas de precios para determinados productos agrícolas, de acuerdo a la estacionalidad, variables exógenas y fluctuaciones del cambio de divisas.
					</p>
				<?php } elseif ($locale=='en_US') { ?>
					<h4>Spot Mode Distribution</h4>
					<p>
						Commercialization and delivery of agricultural products in seasonal frequency. <br>
						This service allows our clients to get access to agricultural product volumes that are part of stock remains. <br>
						It requires a previous purchase agreement that establishes price ranges for determined products, based in its seasonality, external variables and foreign exchange fluctuations.
					</p>
				<?php } elseif ($locale=='zh_CN') { ?>
					<h4>現貨經銷模式</h4>
					<p>
						按季節銷售和交付農産品。此項服務使得客戶能得到部分庫存的農産品。<br>
						此項服務需要提前訂立采購協議，根據采購商品的季節性、外部因素及外彙浮動來確定其價格區間。
					</p>
				<?php } elseif ($locale=='zh_TW') { ?>
					<h4>现货经销模式</h4>
					<p>
						按季节销售和交付农产品。此项服务使得客户能得到部分库存的农产品。 <br>
						此项服务需要提前订立采购协议，根据采购商品的季节性、外部因素及外汇浮动来确定其价格区间。
					</p>
        		<?php } ?>
			</div>
		</div>

		<div class="servicio clearfix">
			<div class="col-sm-2">
				<?= $this->Html->image('servicios/keep-it-freeze.png', ['class' => 'img-responsive img-wide']) ?>
			</div>
			<div class="col-sm-10">
        		<?php if ($locale=='es_CL') {?>
					<h4>Keep it Freeze</h4>
					<p>
						Servicio de bodegaje en cámaras de frío, con atmósfera controlada y temperatura fija, en una ubicación geográfica de alta relevancia estratégica para operaciones de envío de productos congelados al exterior. <br>
						Servicio Idóneo para exportadores, que tienen en nuestra región un punto de conexión rápido y expedito a los principales puertos aéreos y marítimos para el embarque de sus mercaderías.
					</p>
				<?php } elseif ($locale=='en_US') { ?>
					<h4>Keep it Freeze</h4>
						<p>
							Cold storage services in chambers with controlled environment and fixed temperature, in a hightly strategic geographic location for frozen product export operations. <br>
							Ideal service for exporters, having on our region a fast hub to the main air and sea ports for the shipping of their commodities.
						</p>
				<?php } elseif ($locale=='zh_CN') { ?>
					<h4>冷凍貯藏</h4>
						<p>
							在一個對于冷凍産品出口企業的運營戰略有絕佳地理位置提供産品的冷凍儲存， <br> 冷藏室環境可控，恒溫。<br>
							在大區提供給出口商一個舒適的運輸服務，通過大區樞紐快速抵達大型機場和海港，進行貨物裝載。
						</p>
				<?php } elseif ($locale=='zh_TW') { ?>
					<h4>冷冻贮藏</h4>
						<p>
							在一个对于冷冻产品出口企业的运营战略有绝佳地理位置提供产品的冷冻储存， <br> 冷藏室环境可控，恒温。 <br>
							在大区提供给出口商一个舒适的运输服务，通过大区枢纽快速抵达大型机场和海港，进行货物装载。
						</p>
        		<?php } ?>
			</div>
		</div>

		<div class="servicio clearfix">
			<div class="col-sm-2">
				<?= $this->Html->image('servicios/co2-neutral.png', ['class' => 'img-responsive img-wide']) ?>
			</div>
			<div class="col-sm-10">
        		<?php if ($locale=='es_CL') {?>
					<h4>Delivery CO2 Neutral</h4>
					<p>
						Servicio de neutralización de las emisiones de Carbono (CO2) del servicio de Delivery. <br>
						Este servicio permite a los exportadores enviar sus embarques con una parte de su Huella de Carbono neutralizada, lo que es cada vez más valorado en mercados de alta exigencia en lo que refiere a la Responsabilidad Social Empresarial y el desarrollo de modelos sustentables para la gestión alimentaria mundial. <br>
						AndesFrut provee la adquisición de Certificados de Reducción de Emisiones a través de sus alianzas con entidades auditoras que certifican los procesos.
					</p>
				<?php } elseif ($locale=='en_US') { ?>
					<h4>CO2 Neutral Delivery</h4>
					<p>
						This service consists in the neutralization of carbon emissions (CO2) on the delivery services. <br> 
						This allows exporters to ship their products with a part of their carbon footprint neutralized, which is highly valued in high demand markets regarding to social-enterprise responsibility and the development of sustainable models for the world food industry. <br>
						AndesFrut provides the aquisition of Certified Emission Reductions through our partnerships with audit entities that certify the processes.
					</p>
				<?php } elseif ($locale=='zh_CN') { ?>
					<h4>減排運輸方式</h4>
					<p>
						運輸服務采取中和碳排放（CO2）的方式。 <br> 
						此服務可以使出口商在其貨物運輸的過程中減少碳足印的排放，也是日益受到高品質市場所重視的全球食品工業管理中的可持續發展模式和企業社會責任。 <br>
						AndesFrut 可通過其合作的審計機構證實並開具減排證明。
					</p>
				<?php } elseif ($locale=='zh_TW') { ?>
					<h4>减排运输方式</h4>
					<p>
						运输服务采取中和碳排放（CO2）的方式。 <br> 
						此服务可以使出口商在其货物运输的过程中减少碳足印的排放，也是日益受到高品质市场所重视的全球食品工业管理中的可持续发展模式和企业社会责任。 <br>
						AndesFrut可通过其合作的审计机构证实并开具减排证明。
					</p>
        		<?php } ?>
			</div>
		</div>

	</div>
</div>