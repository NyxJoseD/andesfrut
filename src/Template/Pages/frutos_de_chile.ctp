<?= $this->assign('title', '- ' .  __('Frutos de Chile')) ?>

<div class="container-fluid title title-frutos-de-chile">
    <div class="container">
        <h2><?= __('Frutos de Chile') ?></h2>
    </div>
</div>
<div class="container">
    <div class="row">
       <div class="col-sm-6">
        <?php if ($locale=='es_CL') {?>
        <p>
            <span class="font-dark-green">Tierra generosa</span><br>
            que produce <br>
            <span class="font-light-green">frutos incomparables</span><br>
            bajo la protección de <br>
            <span class="font-dark-green">Los Andes milenarios</span>
        </p>
        <p>
            Chile se extiende desde la latitud Sur 17º30´ hasta los 90º.
        </p>
        <p>
            Una tierra generosa, privilegiada en cuanto a sus diferentes climas y tipos de suelo, que le permite entregar una amplia diversidad de frutos de la tierra, con un desarrollo de la industria agrícola en constante crecimiento.
        </p>
        <p>
            Desde los templados valles precordilleranos andinos de Atacama, donde olivos, paltos, kiwis, parronales de uva de mesa, ajíes, tomates y pimientos… dan paso a la templada zona central y sus valles pródigos en carozados, manzanas,     
        </p>
       </div>
       <div class="col-sm-6">
            <p>
                maíz, uvas, melones, naranjas, cebollas, hortalizas, con miles de granjas y huertas que entregan la más exquisita variedad para el deleite de las cocinas del mundo.
            </p>
            <p>
                Desde los frescos valles andinos de la transición, suavemente acariciados por la brisa del Océano Pacífico en las zonas costeras, que nos brindan un exuberante crisol de granos y cereales, legumbres y frutos secos, hasta llegar a la Patagonia Norte, con su sinfonía fresas, grosellas, frambuesas, moras, arándanos, sumados a una amalgama de tubérculos centenarios.
            </p>
            <p>
                Cerrando el recorrido con la Patagonia Media, con sus más de 160 variedades de papas y su enorme surtido de fuentes para la polinización y producción de más de 300 variedades de sabores en la miel natural de abejas.
            </p>
            <p>                
                <span class="font-light-green">Este Chile, protegido por la Cordillera de Los Andes, es la casa de</span> <span class="font-dark-green">AndesFrut</span>
            </p>
        <?php } elseif ($locale=='en_US') { ?>
        <p>
            <span class="font-dark-green">Generous land</span><br>
            that produces <br>
            <span class="font-light-green">incomparable fruits</span><br>
            under the protection of <br>
            <span class="font-dark-green">the ancient Andes</span>
        </p>
        <p>
            Chile extends from latitude 17°30’ south to the 90°.
        </p>
        <p>
            A generous land, privileged regarding to different climates y types of soils, allowing it to give a great diversity of fruits from the ground, with the development of the agriculture industry in constant growth.
        </p>
        <p>
            From the tempered foothills valleys of Atacama, where olives, kiwis, grape vines, tomatoes and peppers let by to the tempered central zone and their prodigious valleys filled with apples,    
        </p>
       </div>
       <div class="col-sm-6">
            <p>
                corn, grapes, melons, oranges, onions and vegetables, with thousands of farms and orchards that deliver the most exquisite variety for the world’s kitchens delight.
            </p>
            <p>
                From the fresh Andean valleys of the transition, softly cuddled by the Pacific Ocean breeze in the coast zones, bringing us an exuberant range of grains, cereals, and dried fruits, reaching the Northern Patagonia, with its symphony of cherries, currants, raspberries, blackberries and blueberries joined to an amalgam of centenary tubercles.
            </p>
            <p>
                Closing our journey at the Middle Patagonia, with more than 160 varieties of potatoes and huge assortment of sources for the pollination and production of more than 300 varieties of flavors of natural bee honey.
            </p>
            <p>                
                <span class="font-light-green">This Chile, protected by the Andes, is the home of</span> <span class="font-dark-green">AndesFrut</span>
            </p>
        <?php } elseif ($locale=='zh_CN') { ?>
        <p>
            <span class="font-dark-green">掩映于千年安第斯山脈</span><br>
            <span class="font-light-green">層巒缭繞下的肥沃土地, </span><br>
            <span class="font-dark-green">出産頂級優質水果。</span>
        </p>
        <p>
            智利位于南緯17°30’至90°之間。
        </p>
        <p>
            得益于智利氣候和土壤的多樣性, 這片肥沃土地出産多種水果, 農 業發展取得持續增長。
        </p>
        <p>
            從阿塔卡馬的安第斯山麓溫帶山谷中, 種植有橄榄、鳄梨、猕猴桃、葡萄、辣椒, 西紅柿和甜椒, 到中部溫帶地區,
        </p>
       </div>
       <div class="col-sm-6">
            <p>
                出産蘋果、玉米、葡萄、甜瓜、橘子、洋蔥和各種蔬菜。當地有成千上萬的農場和果園, 爲全球各地提供種類繁多的頂級精品食材。
            </p>
            <p>
                從空氣清新的安第斯山谷，逐漸過渡到被太平洋溫柔海風環抱著的海岸線一帶，各式各樣、品種繁多的糧食、谷物、豆類和幹果在這裏生長，一直到巴塔哥尼亞北部，還種植有紅加侖、覆盤子、黑莓和藍莓以及其他具有百年種植史的各種小塊莖類農作物。
            </p>
            <p>
                我們在巴塔哥尼亞中部結束了自己的旅程，這裏種植著160多種薯類作物，並且授粉源種類繁多，出産300多種口味的天然蜂蜜。
            </p>
            <p>                
                <span class="font-light-green">智利，在安第斯山脈的保護包繞之下； 這裏，就是</span> <span class="font-dark-green">AndesFrut</span> <span class="font-light-green">之鄉。</span>
            </p>
         <?php } elseif ($locale=='zh_TW') { ?>
        <p>
            <span class="font-dark-green">掩映于千年安第斯山脉</span><br>
            <span class="font-light-green">层峦缭绕下的肥沃土地, </span><br>
            <span class="font-dark-green">出产顶级优质水果。</span>
        </p>
        <p>
            智利位于南纬17°30’至90°之间。
        </p>
        <p>
            得益于智利气候和土壤的多样性，这片肥沃土地出产多种水果，农业发展取得持续增长。
        </p>
        <p>
            从阿塔卡马的安第斯山麓温带山谷中，种植有橄榄、鳄梨、猕猴桃、葡萄、辣椒，西红柿和甜椒，到中部温带地区，出产苹果、
        </p>
       </div>
       <div class="col-sm-6">
            <p>
                玉米、葡萄、甜瓜、橘子、洋葱和各种蔬菜。当地有成千上万的农场和果园，为全球各地提供种类繁多的顶级精品食材。
            </p>
            <p>
                从空气清新的安第斯山谷，逐渐过渡到被太平洋温柔海风环抱着的海岸线一带，，各式各样、品种繁多的粮食、谷物、豆类和干果在这里生长，一直到巴塔哥尼亚北部，还种植有红加仑、覆盘子、黑莓和蓝莓以及其他具有百年种植史的各种小块茎类农作物。
            </p>
            <p>
                我们在巴塔哥尼亚中部结束了自己的旅程，这里种植着160多种薯类作物，并且授粉源种类繁多，出产300多种口味的天然蜂蜜。
            </p>
            <p>                
                <span class="font-light-green">智利，在安第斯山脉的保护包绕之下； 这里，就是</span> <span class="font-dark-green">AndesFrut</span> <span class="font-light-green">之乡。</span>
            </p>
        <?php } ?>
 
       </div> 
    </div>

</div>







