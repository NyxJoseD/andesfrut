<?= $this->assign('title', ' ') ?>
<div class="container-fluid slider">
    <div class="row">
        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                <li><?= $this->Html->image('slides/slide1.jpg') ?></li>
                <li><?= $this->Html->image('slides/slide2.jpg') ?></li>
            </ul></div>
        </div>  

    </div>
</div>

<div class="container home-links">
<div class="row">
    <div class="col-sm-6 col-md-3">
        <?= $this->Html->link(__('Frutos de Chile'),'/fruits-of-chile/',['class' => 'home-1']) ?>
    </div>
    <div class="col-sm-6 col-md-3">
        <?= $this->Html->link(__('Nosotros'),'/about-us/',['class' => 'home-2']) ?>
    </div>
    <div class="col-sm-6 col-md-3">
        <?= $this->Html->link(__('Nuestros Principios'),'/our-principles/',['class' => 'home-4']) ?>
    </div>
    <div class="col-sm-6 col-md-3">
        <?= $this->Html->link(__('Servicios'),'/services/',['class' => 'home-3']) ?>
    </div>
</div></div>

<?= $this->Html->css('/js/wowslider/style', ['block' => 'css']) ?>
<?= $this->Html->script('wowslider/wowslider', ['block' => 'script']) ?>
<?= $this->Html->script('wowslider/script', ['block' => 'script']) ?>