<?= $this->assign('title', '- ' .  __('Nuestros Principios')) ?>

<div class="container-fluid title title-frutos-de-chile">
    <div class="container">
        <h2><?= __('Nuestros Principios') ?></h2>
    </div>
</div>
<div class="container">
	<div class="row principios">
		<div class="col-sm-4">
			<div class="principio mision">
				<div class="titulo-principio">
					<?= __('Misión') ?>
				</div>
				<div class="texto-principio">
						<h2 class="text-center"><?= __('Misión') ?></h2>
					<?php if ($locale=='es_CL'){ ?>
						Chile, potencia agroalimentaria, podrá ser una realidad en la medida que contribuyamos de manera eficiente a la comercialización de productos agrícolas, agregando valor al proceso en su eslabón más importante: el bodegaje y la logística de entrega.
					<?php } elseif ($locale=='en_US'){ ?>
						Chile as an agrifood power could be a reality as long as we contribute efficiently to the commercialization of agricultural products, adding value to the process in its most important link: storage and delivery logistics.
					<?php } elseif ($locale=='zh_CN'){ ?>
						只要我們有效促進農産品的銷售，對其最重要的環節（即儲藏和物流配送）流程增加價值，智利成爲農産品實力強國的願望將會實現。
					<?php } elseif ($locale=='zh_TW'){ ?>
						只要我们有效促进农产品的销售，对其最重要的环节（即储藏和物流配送）流程增加价值，智利成为农产品实力强国的愿望将会实现。
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="principio vision">
				<div class="titulo-principio">
					<?= __('Visión') ?>
				</div>
				<div class="texto-principio">
						<h2 class="text-center"><?= __('Visión') ?></h2>
					<?php if ($locale=='es_CL'){ ?>
						Comercializar productos agrícolas proveyendo los más altos estándares de calidad en las etapas de compra, bodegaje, mantención, logística y delivery.
					<?php } elseif ($locale=='en_US'){ ?>
						Commercialize agricultural products providing the higher quality standards in the purchase, storage, maintenance, logistics and delivery stages.
					<?php } elseif ($locale=='zh_CN'){ ?>
						對農産品實行商業化運作，在采購、儲藏、維護和物流配送環節提供更高質量標准。
					<?php } elseif ($locale=='zh_TW'){ ?>
						对农产品实行商业化运作，在采购、储藏、维护和物流配送环节提供更高质量标准。
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="principio valores">
				<div class="titulo-principio">
					<?= __('Valores') ?>
				</div>
				<div class="texto-principio">
						<h2 class="text-center"><?= __('Valores') ?></h2>
					<?php if ($locale=='es_CL'){ ?>
						La calidad de un producto agrícola tiene su origen en la genética de la especie.<br>
						Se nutre de la calidad de la tierra y de los agricultores.<br>
						Se potencia con la calidad del proceso de comercialización y logística.
					<?php } elseif ($locale=='en_US'){ ?>
						The quality of a agricultural product has its origin in the genetics of the species. <br>
						It is nourished with the quality of the soil and the agricultrists. <br>
						It is powered by the quality of the commercialization and logistics process.
					<?php } elseif ($locale=='zh_CN'){ ?>
						農産品的質量與其物種遺傳學息息相關。 <br>
						土壤質量和農學家的專業技術滋養著它。 <br>
						高質量商業化運作和物流流程是它的生長動力。
					<?php } elseif ($locale=='zh_TW'){ ?>
						农产品的质量与其物种遗传学息息相关。 <br>
						土壤质量和农学家的专业技术滋养着它。 <br>
						高质量商业化运作和物流流程是它的生长动力。
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<br>
	<div class="col-sm-6 col-sm-offset-3">
		<?= $this->Html->image('logo-with-slogan.png', ['class' => 'img-responsive img-wide']) ?>
	</div>
</div>