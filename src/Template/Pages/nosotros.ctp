<?= $this->assign('title', '- ' .  __('Nosotros')) ?>

<div class="container-fluid title title-frutos-de-chile">
    <div class="container">
        <h2><?= __('Nosotros') ?></h2>
    </div>
</div>
<div class="container">
	<div class="row text-justify">
		<div class="col-sm-6">
		<?php if ($locale=='es_CL') {?>
			<p class="font-light-green" style="font-size:16px;">
				AndesFrut es una empresa de comercialización, logística y asesoramiento, en la compra y distribución de productos agrícolas, para la industria y/o empresas de servicios alimentarios.
			</p>
			<p class="font-dark-green">
				La Calidad es Nuestro Objetivo Primario
			</p>
			<p>
				Todos los productos agrícolas, sean estos frutas, hortalizas, verduras, cereales, o cualquier fruto del trabajo del hombre en nuestros campos,  deben responder a nuestros rigurosos estándares de calidad. Tanto en calibre, como notas de maduración, tiempo de cosecha y certificación de origen.
			</p>
			<p>
				Desde la perspectiva logística, monitoreamos y exigimos calidad en la manipulación, transporte, refrigeración, empaque y mantención.
			</p>
			<p>
				Y en lo referido a nuestros equipos de trabajo, cuidamos al máximo la calidad de nuestros colaboradores, técnicos y profesionales, así como cuidamos la calidad de sus beneficios y la relación con las comunidades en las que desarrollamos nuestro quehacer.
			</p>
		<?php } elseif ($locale=='en_US') { ?>
			<p class="font-light-green" style="font-size:16px;">
				AndesFrut is a company specialized in commercialization, logistics and assessment, regarding to purchase and distribution of agricultural products for the alimentary services industry and business. 
			</p>
			<p class="font-dark-green">
				Quality is our primary objective
			</p>
			<p>
				All agricultural products, whether they are fruits, vegetables, cereals or any produce of the labor of a man in our fields, they must respond to our though quality standards. Both caliber as maturing notes, harvest time and certification of origin.
			</p>
			<p>
				From the logistics perspective, we track and demand quality in the manipulation, transport, refrigeration, packing and maintenance.
			</p>
			<p>
				Regarding our work teams, we take maximum care of the quality of our collaborators, technicians and professionals, as much as we care for the quality of their benefits and the relation with the communities in which we develop our job.
			</p>
		<?php } elseif ($locale=='zh_CN') { ?>
			<p class="font-light-green" style="font-size:16px;">
				AndesFrut是一家專門針對飲食服務行業的公司，其中的服務包括有農産品的生産、采購與銷售、物流配送以及咨詢。
			</p>
			<p class="font-dark-green">
				品質是我們追求的首要目標
			</p>
			<p>
				所有農産品，包括水果、蔬菜、谷物或這片土地上的任何勞動成果，都必須完全符合我們的質量標准。同時要求標注其成熟時間和采摘時間並附原産地證明。
			</p>
			<p>
				從物流配送角度來說，我們在操作、運輸、冷藏、包裝和維護方面對産品進行嚴格的質量跟蹤和控制。
			</p>
			<p>
				工作團隊方面，我們極力重視我們合作方、技術人員以及專業人員的素質，正如我們關心他們的利益以及與我們開展工作的社區的關系。
			</p>
		<?php } elseif ($locale=='zh_TW') { ?>
			<p class="font-light-green" style="font-size:16px;">
				AndesFrut 是一家专门针对饮食服务行业的公司，其中的服务包括有农产品的生产、采购与销售、物流配送以及咨询。
			</p>
			<p class="font-dark-green">
				品质是我们追求的首要目标
			</p>
			<p>
				所有农产品，包括水果、蔬菜、谷物或这片土地上的任何劳动成果，都必须完全符合我们的质量标准。同时要求标注其成熟时间和采摘时间并附原产地证明。
			</p>
			<p>
				从物流配送角度来说，我们在操作、运输、冷藏、包装和维护方面对产品进行严格的质量跟踪和控制。
			</p>
			<p>
				工作团队方面，我们极力重视我们合作方、技术人员以及专业人员的素质，正如我们关心他们的利益以及与我们开展工作的社区的关系。
			</p>
		<?php }?>
		</div>
		<div class="col-sm-5 col-sm-offset-1">
			<?= $this->Html->image('nosotros.jpg', ['class' => 'img-responsive img-wide']) ?>
		</div>

	</div>
</div>